FROM cimg/python:3.8
# @see: https://github.com/CircleCI-Public/cimg-python

RUN NODE_VERSION="12.19.0" && \
	curl -L -o node.tar.xz "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz" && \
	sudo tar -xJf node.tar.xz -C /usr/local --strip-components=1 && \
	rm node.tar.xz && \
	sudo ln -s /usr/local/bin/node /usr/local/bin/nodejs

RUN sudo npm install -g serverless

RUN pip install awscli

CMD ["bash"]
